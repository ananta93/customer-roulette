from google.cloud import datastore
import random
from google.cloud.datastore import entity
import schedule
import time

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

datastore_client = datastore.Client(project='my-projects-nodejs')

def get_customer():
    query = datastore_client.query(kind='CustomerKind')
    results = list(query.fetch())
    chosen_customer = random.choice(results)
    print(chosen_customer["email"])

    ####### Sending mail to the random customer ##########

    gmail_user = 'iamanantadas.info@gmail.com'
    gmail_pass = ''

    msg = MIMEMultipart()
    sent_from = gmail_user
    sent_to = chosen_customer["email"]
    msg['From'] = sent_from
    msg["To"] = sent_to
    msg['Subject'] = "Congratulation on your gift card"

    htmlEmail = """
    <p> Hey, <br/><br/>
        "Congratulation!!! You have won a GIFT CARD worth 100$<br/><br/>
    <br/></p>
        Thank you! <br/><br/>
        Best Regards, <br/>
        ABC Retail </p>
    <br/><br/>
    <font color="red">Please do not reply to this email as it is auto-generated via GCP. </font>
    """

    try:
        msg.attach(MIMEText(htmlEmail, 'html'))
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_pass)
        text = msg.as_string()
        server.sendmail(sent_from, sent_to, text)
        server.close()
        print('Email sent successfully !!!')
    except Exception as exception:
        print("Error occurred while sending the mail: %s!\n\n" % exception)
    

schedule.every(60).minutes.do(get_customer)
while 1:
    schedule.run_pending()
    time.sleep(1)
