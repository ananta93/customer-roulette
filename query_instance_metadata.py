import requests
import json

def extract_json(url, path):
    result = {}
    for i in path:
        url_i = url + i
        r = requests.get(url_i)
        text = r.text   #Content of the response, in unicode.
        if i[-1] == "/":                # [-1] means the last element in a sequence. It check if last element is "/"
            values = r.text.splitlines()    #Return a list of the lines in the string, 
            result[i[:-1]] = extract_json(url_i, values)    # It selects all the element of a sequence except the last element. It slices the string to omit the last character
        elif is_valid(text):
            result[i] = json.loads(text)    #Deserializes a JSON object to a standard python object.
        else:
            result[i] = text
    return result

def instance_metadata():
    result = extract_json('http://169.254.169.254/latest/', 'meta-data/') # To view all categories of instance metadata from within a running instance, use the IPv4 URI
    return result

def instance_metadata_json():
    md = instance_metadata()
    instance_metadata_json = json.dumps(md, indent=4, sort_keys=True)   #The json.dumps() method allows us to convert a python object into an equivalent JSON object. Indentation can be used for pretty-printing
    return instance_metadata_json

def is_valid(text):
    try:
        json.loads(text)
    except ValueError:  #ValueError in Python is raised when an invalid value to a function is given
        return False
    return True

def extract_key(key, metadata):     #Find all occurrences of a key in nested dictionaries and lists
    if hasattr(metadata, 'items'):  #Return whether the object has an attribute with the given name.
        for k, v in metadata.items():
            if k == key:
                yield v
            if isinstance(v, dict):     #Return whether an object is in the dictionary
                for result in extract_key(key, v):
                    yield result
            elif isinstance(v, list):      ##Return whether an object is in the list
                for d in v:
                    for result in extract_key(key, d):
                        yield result

def get_key(key):
    metadata = instance_metadata()
    return list(extract_key(key, metadata))

if __name__ == '__main__':
    key = input("Input the key: \n")
    print(get_key(key))  
    print(instance_metadata_json())