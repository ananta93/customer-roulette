from flask import Flask
from flask_restful import Resource, Api, reqparse
from google.cloud import datastore
import re

app = Flask(__name__)
api = Api(app)

datastore_client = datastore.Client(project='my-projects-nodejs')

class Customers(Resource):
    
    def post(self):
        parser = reqparse.RequestParser() 
        parser.add_argument('customerId', required=True)
        parser.add_argument('name', required=True)
        parser.add_argument('email', required=True)
        parser.add_argument('city', required=True)
        parser.add_argument('mobile', required=True)
        args = parser.parse_args()
        
        regex_email = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        regex_mobile = r'(0|91)?[7-9][0-9]{9}'

        def validate_email(email):
            if(re.fullmatch(regex_email, email)):
                return "valid"
            else:
                return "invalid"
        
        def validate_mobile(mobile):
            if (re.fullmatch(regex_mobile, mobile)):
                return "valid"     
            else :
                return "invalid"
    
        status_email = validate_email(args['email'])
        status_mobile = validate_mobile(args['mobile'])

        if(status_email == 'valid' and status_mobile == 'valid'):

            entity = datastore.Entity(key=datastore_client.key('CustomerKind'))
            entity.update({
                'customerId': args['customerId'],
                'name': args['name'],
                'email': args['email'],
                'city': args['city'],
                'mobile': args['mobile']
            })
            datastore_client.put(entity)
            return ('Record Insertion was successful', 200)
        
        elif(status_email != 'valid' and status_mobile != 'valid' ):
            return ('Invalid email and mobile number entered'), 200
        elif(status_email != 'valid'):
            return ('Invalid email address'), 200
        else:
            return ('Invalid mobile number'), 200

api.add_resource(Customers, '/customers')

if __name__ == '__main__':
    app.run()
