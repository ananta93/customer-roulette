class dict2obj(object):     #Class creates a user-defined data structure, which holds its own data members and member functions,
                            # which can be accessed and used by creating an instance of that class.
    def __init__(self, d):  #__init__  function is called every time an object is created from a class.
        self.__dict__['object'] = d     #The self parameter is a reference to the current instance of the class, and is used to access variables that belongs to the class.

    def __getattr__(self, key):     #Called when an attribute lookup has not found the attribute in the usual places (i.e. it is not an instance attribute nor is it found in the class tree for self).
        value = self.__dict__['object'][key]        #__dict__ is A dictionary or other mapping object used to store an object's (writable) attributes. Or speaking in simple words every object in python has an attribute which is denoted by __dict__. 
        if type(value) == type({}):     #it checks if the value is of dictionary type
            return dict2obj(value)
        return value

k1=input("Please input the first key: ") 
k2=input("Please input the second key: ") 
k3=input("Please input the third key: ") 
v2=input("Plesse input the value: ") 
object={k1:{k2:{k3:v2}}}

value3 = dict2obj(object)
print("The Value is: "+value3.a.b.c)